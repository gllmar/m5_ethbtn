# M5_ethbtn


## Branchements

### M5 vers BTN

```mermaid
flowchart LR
    subgraph M5
        M5_GND(GND:NOIR)
        M5_VCC(VCC:ROUGE)
        M5_26(26:JAUNE)
        M5_32(32:BLANC)
        10kΩ
    end

    subgraph BOUTON
        BTN_NO
        BTN_NC
        BTN_COMMON
    end

   M5_VCC-->10kΩ-->M5_32-->BTN_NO
   M5_GND-->BTN_COMMON

```
### Adaptateur Groove->pins

![](img/adaptateurs.jpg)