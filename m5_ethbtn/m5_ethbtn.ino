/*
distibuted-memories 
implementation bouton osc broadcast
*/
//  OSC MESSAGE
char * osc_prefix="/m5_ethbtn/btn";
#define OSC_OUT_PORT 50001
//



// ethPOE
#include <SPI.h>
#include <Ethernet2.h>


#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19

//Changer les derniers "chiffres de la mac addresse"
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xFE };
IPAddress ip_broadcast;

// \ethPOE

// osc
#include <OSCMessage.h>
#include <EthernetUdp2.h>
EthernetUDP udp;
const unsigned int osc_out_port = OSC_OUT_PORT;

// \osc


// m5 LED
#include "M5Atom.h"

// couleur du led
#define RED 0xff, 0x00, 0x00
#define YELLOW 0xff, 0xff, 0x00
#define GREEN 0x00, 0xff, 0x00
#define CYAN 0x00, 0xff, 0xff
#define BLUE 0x00, 0x00, 0xff
#define PINK 0xff, 0x00, 0xff
#define OFF 0x00, 0x00, 0x00



uint8_t DisBuff[2 + 5 * 5 * 3];
void set_m5_led(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
    DisBuff[0] = 0x05;
    DisBuff[1] = 0x05;
    for (int i = 0; i < 25; i++)
    {
        DisBuff[2 + i * 3 + 1] = Rdata; // -> fix : grb? weird?
        DisBuff[2 + i * 3 + 0] = Gdata; //->  fix : grb? weird? 
        DisBuff[2 + i * 3 + 2] = Bdata;
    }
      M5.dis.displaybuff(DisBuff);
}

// \m5 LED


//  btn externe
#define btn_pin 32
Button bouton = Button(btn_pin, true, 100);

// \btn externe


void setup() {
  M5.begin(true, false, true);
  set_m5_led(RED);

  

  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);

  // init DHCP
  Ethernet.begin(mac);
  udp.begin(8888);
  set_m5_led(PINK); // IF here; ethernet initialize
  

  // Create broadcast IP
  ip_broadcast = Ethernet.localIP();
  ip_broadcast[3]=255;

  // Print init state
  Serial.print("Button ip => ");
  Serial.print(Ethernet.localIP());
  Serial.print(" broadcasting => ");
  Serial.println(ip_broadcast);

}

void loop() 
{
  // listen for incoming clients
   M5.update();
   bouton.read();
   if (M5.Btn.wasPressed())
   {
    Serial.println("BTN");
    send_osc_btn(1);
    cycle_m5_color(200);
   }

   if(bouton.wasPressed())
   {
     send_osc_btn(1);
   }
   if(bouton.wasReleased())
   {
         send_osc_btn(0);
   }
}


void send_osc_btn(bool b_state)
{

  //Serial.println(OSC_PREFIX);
  
  OSCMessage msg(osc_prefix);
  msg.add((int32_t)b_state); 
  msg.add(((const char *)"time"));
  msg.add(((int32_t)millis()));
  udp.beginPacket(ip_broadcast, osc_out_port);
  msg.send(udp); // send the bytes to the SLIP stream
  udp.endPacket(); // mark the end of the OSC Packet
  msg.empty(); // free space occupied by message
}


void cycle_m5_color(int d_time)
{
  delay(d_time);
  set_m5_led(RED);
  delay(d_time);
  set_m5_led(YELLOW);
  delay(d_time);
  set_m5_led(GREEN);
  delay(d_time);
  set_m5_led(CYAN);
  delay(d_time);
  set_m5_led(BLUE);
  delay(d_time);
  set_m5_led(PINK);
}
